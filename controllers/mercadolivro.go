package controllers

import (
	"cep/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func BuscaCepMercadolivre(cep string) *models.Cidade {
	fmt.Println("fim de percurso ML")
	fmt.Println(cep)

	resp, err := http.Get("https://addresses.mercadolivre.com.br/getZipCodeData?zipCode=" + cep)
	if err != nil {
		fmt.Println("Erro ao obter resposta", err.Error())
		return nil
	}
	fmt.Println("hey1")

	if resp.StatusCode != 200 {
		return nil
	}
	fmt.Println("hey2")

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("erro em ReadAll", err.Error())
		return nil
	}

	defer resp.Body.Close()
	data = toUTF8(data)

	var cidadeinfo models.CidadeMercado
	err = json.Unmarshal([]byte(data), &cidadeinfo)
	if err != nil {
		fmt.Println("erro em ReadAll", err.Error())
		return nil
	}
	fmt.Println("hey3")

	cidadeinfo.Estado.Id = strings.ReplaceAll(cidadeinfo.Estado.Id, "BR-", "")

	return &models.Cidade{
		Cep:         cidadeinfo.Cep,
		Cidade:      cidadeinfo.Cidade.Name,
		Logradouro:  cidadeinfo.DemaisAtributos.Endereco,
		Complemento: cidadeinfo.DemaisAtributos.NumerdoEndereco,
		Bairro:      cidadeinfo.DemaisAtributos.Bairro,
		Uf:          cidadeinfo.Estado.Id,
	}

}

func toUTF8(iso8859_1_buf []byte) []byte {
	buf := make([]rune, len(iso8859_1_buf))
	for i, b := range iso8859_1_buf {
		buf[i] = rune(b)
	}
	return []byte(string(buf))
}
