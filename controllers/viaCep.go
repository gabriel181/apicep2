package controllers

import (
	"cep/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func BuscaCepViaCep(cep string) *models.Cidade {
	fmt.Println("hey")
	resp, err := http.Get("https://viacep.com.br/ws/" + cep + "/json")
	if err != nil {
		fmt.Println("Erro ao obter resposta", err.Error())
		return nil
	}

	if resp.StatusCode != 200 {
		return nil
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("erro em ReadAll", err.Error())
		return nil
	}

	defer resp.Body.Close()
	var cidadeinfo models.CidadeViaCep
	err = json.Unmarshal([]byte(data), &cidadeinfo)
	if err != nil {
		fmt.Println("erro em ReadAll", err.Error())
		return nil
	}

	if cidadeinfo.Erro {
		return nil
	}

	return &models.Cidade{
		Cep:         cidadeinfo.Cep,
		Logradouro:  cidadeinfo.Cidade,
		Complemento: cidadeinfo.Complemento,
		Bairro:      cidadeinfo.Bairro,
		Uf:          cidadeinfo.Uf,
		Ibge:        cidadeinfo.Ibge,
		Ddd:         cidadeinfo.Ddd,
	}

}
