package main

import (
	"cep/controllers"
	"fmt"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.GET("/cep", controllers.Cep)
	//r.GET("/cep", contro)

	if erro := r.Run(":8081"); erro != nil {
		fmt.Print("Erro ao conectar a rota à um http.server")
	}
}
