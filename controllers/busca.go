package controllers

import (
	"log"
	"regexp"
	"strings"

	"github.com/gin-gonic/gin"
)

func Cep(c *gin.Context) {
	cep := c.Query("cep")
	cep = strings.ReplaceAll(cep, "-", "")
	cep = strings.ReplaceAll(cep, ".", "")

	rx := regexp.MustCompile(`^[0-9]{8}$`)

	if !rx.Match([]byte(cep)) {
		c.JSON(400, gin.H{
			"message ": "CEP Inválido",
		})
		return
	}

	data := BuscaCepViaCep(cep)
	log.Println("struct viacep: ")
	if data != nil {
		c.JSON(200, gin.H{"data": data})
		return
	}

	data = BuscaCepCepAberto(cep)
	log.Println("struct cepAberto: ")
	if data != nil {
		c.JSON(200, gin.H{"data": data})
		return
	}

	data = BuscaCepMercadolivre(cep)
	log.Println("struct MercadoLivre:")
	if data != nil {
		c.JSON(200, gin.H{"data": data})
		return
	}

	c.AbortWithStatusJSON(400, gin.H{"error": "CEP não encontrado"})
}
