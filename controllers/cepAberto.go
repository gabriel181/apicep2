package controllers

import (
	"cep/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func BuscaCepCepAberto(cep string) *models.Cidade {
	// Create a key-value list of headers
	url := "https://www.cepaberto.com/api/v3/cep?cep=" + cep

	// Create a new request using http
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println("erro em http.:", err)
		return nil
	}

	// add authorization header to the req
	req.Header.Add("Authorization", "Token token=7ff3bc319c4dd003ed0c91b3874b5bc1")

	// Send req using http Client
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Println("parar on response.\n[paraR] -", err)
		return nil
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("parar while reading the response bytes:", err)
		return nil
	}

	if string(body) == "{}" {
		return nil
	}

	log.Println(string([]byte(body)))
	fmt.Println("dentro de cepAberto", string([]byte(body)))
	var cidadeinfo2 models.CidadeAberto
	err = json.Unmarshal([]byte(body), &cidadeinfo2)
	if err != nil {
		fmt.Println("erro em ReadAll", err.Error())
		return nil
	}

	return &models.Cidade{
		Cep:         cidadeinfo2.Cep,
		Cidade: cidadeinfo2.Cidade.Nome,
		Logradouro:  cidadeinfo2.Logradouro,
		Complemento: cidadeinfo2.Complemento,
		Bairro:      cidadeinfo2.Bairro,
		Uf:          cidadeinfo2.Estado.Sigla,
		Ddd:         cidadeinfo2.Ddd,
	}
}
