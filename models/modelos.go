package models

//Recebe informaçoes de ViaCep
type CidadeViaCep struct {
	Cep         string `json:"cep"`
	Logradouro  string `json:"logradouro"`
	Complemento string `json:"complemento"`
	Bairro      string `json:"bairro"`
	Cidade      string `json:"localidade"`
	Uf          string `json:"uf"`
	Ibge        string `json:"ibge"`
	Gia         string `json:"gia"`
	Ddd         string `json:"ddd"`
	Siafi       string `json:"siafi"`
	Erro        bool   `json:"erro"`
}

/*func (c *CidadeViaCep) ToCidade() Cidade{
return Cidade {
	Cep: c.Cep
	Logradouro
	Complemento
	Bairro
	Localidade
	Uf
	Ibge
	Ddd

}
*/
//CidadeAberto recebe as informaçoes de CEPABERTO
type CidadeAberto struct {
	Cidade      CidadeCidadeAberto `json:"cidade"`
	Estado      Estado             `json:"estado"`
	Altitude    float64            `json:"altitude"`
	Cep         string             `json:"cep"`
	Latitude    string             `json:"latitude"`
	Longitude   string             `json:"longitude"`
	Bairro      string             `json:"bairro"`
	Logradouro  string             `json:"logradouro"`
	Complemento string             `json:"complemento"`
	Ddd         string             `json:"ddd"`
}
type CidadeCidadeAberto struct {
	Ddd  int    `json:"ddd"`
	Nome string `json:"nome"`
	Ibge string `json:"ibge"`
}
type Estado struct {
	Sigla string `json:"sigla"`
}

//Busca na api do MLivre
type CidadeMercado struct {
	Cep             string             `json:"zip_code"`
	Cidade          City               `json:"city"`
	Estado          State              `json:"state"`
	Pais            Country            `json:"country"`
	DemaisAtributos ExtendedAttributes `json:"extended_attributes"`
}

type City struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}
type State struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}
type Country struct {
	Id   string `json:"id"`
	Nome string `json:"name"`
}
type ExtendedAttributes struct {
	Endereco        string  `json:"address"`
	Proprietario    string  `json:"owner_name"`
	TipoDeCep       TipoCep `json:"zip_code_type"`
	TipoCidade      string  `json:"city_type"`
	Bairro          string  `json:"neighborhood"`
	Status          string  `json:"status"`
	NumerdoEndereco string  `json:"addressNumber"`
}
type TipoCep struct {
	Tipo      string `json:"type"`
	Descricao string `json:"description"`
}

//Uniao dos modelos
type Cidade struct {
	Cep         string `json:"cep"`
	Cidade      string `json:"cidade"`
	Logradouro  string `json:"logradouro"`
	Bairro      string `json:"bairro"`
	Localidade  string `json:"localidade"`
	Complemento string `json:"complemento"`
	Uf          string `json:"uf"`
	Ibge        string `json:"ibge"`
	Ddd         string `json:"ddd"`
}
